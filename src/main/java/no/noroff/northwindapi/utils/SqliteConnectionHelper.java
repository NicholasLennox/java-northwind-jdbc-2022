package no.noroff.northwindapi.utils;

import org.springframework.stereotype.Component;

@Component
public class SqliteConnectionHelper implements ConnectionHelper {
    @Override
    public String getConnectionString() {
        return "jdbc:sqlite:src/main/resources/Northwind_small.sqlite";
    }
}
