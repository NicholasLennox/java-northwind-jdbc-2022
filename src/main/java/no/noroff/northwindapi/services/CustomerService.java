package no.noroff.northwindapi.services;

import no.noroff.northwindapi.models.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> getAll();
    Customer getById();
    int add();
    int update();
    int delete();
}
