package no.noroff.northwindapi.utils;

public interface ConnectionHelper {
    String getConnectionString();
}
