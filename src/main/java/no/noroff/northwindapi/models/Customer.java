package no.noroff.northwindapi.models;

public record Customer(String Id, String CompanyName, String ContactName, String City) {

}
