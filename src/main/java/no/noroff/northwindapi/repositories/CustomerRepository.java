package no.noroff.northwindapi.repositories;

import no.noroff.northwindapi.models.Customer;

import java.util.List;

public interface CustomerRepository {
    List<Customer> getAll();
    Customer getById();
    int add();
    int update();
    int delete();
}
