package no.noroff.northwindapi.dao;

import no.noroff.northwindapi.models.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAO {
    private final String connectionString = "jdbc:sqlite:src/main/resources/Northwind_small.sqlite";

    public List<Customer> getAllCustomers() {
        List<Customer> returnCustomers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(connectionString)) {
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Id,CompanyName, ContactName, City FROM customer");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                returnCustomers.add(
                        new Customer(
                                resultSet.getString("Id"),
                                resultSet.getString("CompanyName"),
                                resultSet.getString("ContactName"),
                                resultSet.getString("City")
                        ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnCustomers;
    }

    public Customer getCustomerById(String id) {
        Customer returnCustomer = null;
        try(Connection conn = DriverManager.getConnection(connectionString)) {
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Id,CompanyName, ContactName, City FROM customer WHERE Id = ?");
            preparedStatement.setString(1,id);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                returnCustomer = new Customer(
                                resultSet.getString("Id"),
                                resultSet.getString("CompanyName"),
                                resultSet.getString("ContactName"),
                                resultSet.getString("City")
                        );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnCustomer;
    }

    public int addCustomer(Customer customer) {
        int result = 0;
        try(Connection conn = DriverManager.getConnection(connectionString)) {
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer(Id,CompanyName,ContactName,City) VALUES(?,?,?,?)");
            preparedStatement.setString(1,customer.Id());
            preparedStatement.setString(2,customer.CompanyName());
            preparedStatement.setString(3,customer.ContactName());
            preparedStatement.setString(4,customer.City());
            // Execute Query
            result = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
