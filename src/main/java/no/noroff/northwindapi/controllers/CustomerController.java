package no.noroff.northwindapi.controllers;

import no.noroff.northwindapi.dao.CustomerDAO;
import no.noroff.northwindapi.models.Customer;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/customers")
public class CustomerController {

    CustomerDAO customerDAO = new CustomerDAO();

    @GetMapping
    public List<Customer> getAll() {
        return customerDAO.getAllCustomers();
    }

    @GetMapping("{id}")
    public Customer getById(@PathVariable String id) {
        return customerDAO.getCustomerById(id);
    }

    @PostMapping
    public int add(@RequestBody Customer customer) {
        return customerDAO.addCustomer(customer);
    }
}
