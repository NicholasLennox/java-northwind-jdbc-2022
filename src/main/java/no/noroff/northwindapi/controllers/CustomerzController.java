package no.noroff.northwindapi.controllers;

import no.noroff.northwindapi.models.Customer;
import no.noroff.northwindapi.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v2/customers")
public class CustomerzController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public List<Customer> getAll() {
        return customerService.getAll();
    }

    @GetMapping("{id}")
    public Customer getById(@PathVariable String id) {
        return customerService.getById();
    }
}
